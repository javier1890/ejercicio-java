package diagrama_flujo;

import java.util.Scanner;

public class Ejercicio05 {

	public static void main(String[] args) {
		// 5.- Hacer un diagrama de flujo para sumar los N primeros n�meros impares.
		// Realizar luego uno que haga lo mismo pero con los n�meros pares, otro con
		// los m�ltiplos de 3 y finamente otro con los m�ltiplos de 5..
		int num1, op;
		Scanner leer = new Scanner(System.in);

		System.out.println("|*******************MENU*********************|");
		System.out.println("|1)- N numeros impares             **********|");
		System.out.println("|2)- N numeros pares               **********|");
		System.out.println("|3)- N numeros multiplos de 3      **********|");
		System.out.println("|4)- N numeros multiplos de 5      **********|");
		System.out.println("| Para salir presiones cualquier tecla   ****|");
		op = (int)leer.nextInt();

		switch (op) {
		case 1:
			System.out.println("Ingrese N� el limite para numeros pares:");
			num1 = leer.nextInt();
			impares(num1);
			break;

		case 2:
			System.out.println("Ingrese N� el limite para numeros impares:");
			num1 = leer.nextInt();
			pares(num1);
			break;

		case 3:
			System.out.println("Ingrese N� el limite para numeros multiplos de 3:");
			num1 = leer.nextInt();
			multiplo3(num1);
			break;
		case 4:
			System.out.println("Ingrese N� el limite para numeros multiplos de 3:");
			num1 = leer.nextInt();
			multiplo5(num1);
			break;

		default:
			System.out.println("muchas gracias , adios!!:");
			
			break;
		}

	}

	public static void impares(int n) {
		for (int i = 0; i < n; i++) {
			if (i % 2 != 0) {
				System.out.println(i);
			}
		}
	}

	public static void pares(int n) {
		int i = 1;
		do {
			if (i % 2 == 0) {
				System.out.println(i);
				
			}
			i++;
		} while (i < n);
	}

	public static void multiplo3(int n) {
		for (int i = 0; i < n; i++) {
               if (i%3==0) {
            	   System.out.println(i);
			}
		}
	}

	public static void multiplo5(int n) {

		 for (int i = 0; i < n; i++) {
			if (i%5==0) {
				System.out.println(i);
			}
		}
	}

}
