package diagrama_flujo;

import java.util.Scanner;

public class Ejercicio06 {
      //fibonacci
	public static void main(String[] args) {
		int n;
	   Scanner leer=new Scanner(System.in);
	   
       System.out.println("Ingrese el numero N de la serie de fibonacci:");
       n=leer.nextInt();
       
       
       System.out.println("\n");
       for (int i = 0; i < n ;i++) {
		System.out.println(fibonacci(i)+"  ");
	}
       System.out.println();
	}
	
	public static int fibonacci(int n) {		
		if (n>1) {
			return fibonacci(n-1)+fibonacci(n-2);
		}
		else if (n==1) {
			return 1;
		}
		else if (n==0) {
			return 0;
		}
		else {
			System.out.println("tienes que ingresar un numero de tama�o mayor o igual a 1");
	        return 0; 
		}
	}

}
