package diagrama_flujo;

import java.util.Scanner;

public class Ejercicio07 {

	public static void main(String[] args) {
		// - Realizar un diagrama de flujo que lea N n�meros, calcule y escriba la suma
		// de los pares y el producto de los impares.

		int n;
		int suma_par=0;
		int suma_impar=0;
		Scanner leer = new Scanner(System.in);

		System.out.println("Ingrese el numero N:");
		n = leer.nextInt();

		for (int i = 0; i < n; i++) {
			if (i%2==0) {
				suma_par=suma_par+i;
			}
			else {
				suma_impar=suma_impar+i;
			}
		}
		
		System.out.println("La suma de pares es:"+suma_par);
		System.out.println("La suma de los impares es:"+suma_impar);
	}

}
