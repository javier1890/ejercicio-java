package diagrama_flujo;

import java.util.Scanner;

public class Ejercicio09 {

	public static void main(String[] args) {
//		9.- Hace run diagrama de flujo para calcular el m�ximo com�n divisor de dos
//		n�meros enteros positivos N y M siguiendo el algoritmo de Euclides, que es
//		el siguiente:

		
		int n,m,dividir;
		Scanner leer = new Scanner(System.in);

		System.out.println("Ingrese el numero N:");
		n = leer.nextInt();
		
		System.out.println("Ingrese el numero M:");
		m = leer.nextInt();
		
		mcd(n,m);
	}

	
	public static void mcd(int n,int m) {
		int resto=0;
		if (n>0 && m >0) {
			resto=n/m;
			
			if (resto==0) {
				System.out.println("El maximo cmun divisor es:"+m);
				
			}else {
				n=m;
				m=resto;
				mcd(n,m);
			}	
		}else {
			System.out.println("los valores ingresados son incorrectos ");
		}
		
	}
}
